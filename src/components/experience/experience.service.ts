import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Resume } from '../resume/entities/resume.entity';
import { CreateExperienceDto } from './dto/create-experience.dto';
import { UpdateExperienceDto } from './dto/update-experience.dto';
import { Experience } from './entities/experience.entity';
import * as fs from 'fs';

@Injectable()
export class ExperienceService {
  constructor(
    @InjectRepository(Experience)
    private experienceRepository: Repository<Experience>,
  ) {}

  create(createExperienceDto: CreateExperienceDto, file: Express.Multer.File) {
    const experience = new Experience();

    const resume = new Resume();
    resume.id = createExperienceDto.resumeId;

    experience.companyLogo = `http://localhost:3000/file/${file.filename}`;
    experience.company = createExperienceDto.company;
    experience.description = createExperienceDto.description;
    experience.startDate = createExperienceDto.startDate;
    experience.endDate = createExperienceDto.endDate;
    experience.endDate = createExperienceDto.endDate;
    experience.jobTitle = createExperienceDto.jobTitle;
    experience.resume = resume;

    return this.experienceRepository.save(experience);
  }

  findAll(id: number) {
    return this.experienceRepository.find({
      where: [
        {
          resume: {
            id,
          },
        },
      ],
    });
  }

  async update(
    id: number,
    updateExperienceDto: UpdateExperienceDto,
    file: Express.Multer.File,
  ) {
    const beforeUpdate = await this.experienceRepository.findOne({
      where: {
        id,
      },
    });

    updateExperienceDto.company
      ? (beforeUpdate.company = updateExperienceDto.company)
      : null;

    updateExperienceDto.description
      ? (beforeUpdate.description = updateExperienceDto.description)
      : null;

    updateExperienceDto.startDate
      ? (beforeUpdate.startDate = updateExperienceDto.startDate)
      : null;

    updateExperienceDto.endDate
      ? (beforeUpdate.endDate = updateExperienceDto.endDate)
      : null;

    updateExperienceDto.jobTitle
      ? (beforeUpdate.jobTitle = updateExperienceDto.jobTitle)
      : null;

    if (file) {
      this.deleteFile(
        beforeUpdate.companyLogo.split('/')[
          beforeUpdate.companyLogo.split('/').length - 1
        ],
      );
      beforeUpdate.companyLogo = `http://localhost:3000/file/logos/${file.filename}`;
    }

    return this.experienceRepository.upsert(beforeUpdate, ['id']);
  }

  async remove(id: number) {
    const ex = await this.experienceRepository.findOne({
      where: { id },
    });

    if (ex && ex.companyLogo && ex.companyLogo.length) {
      this.deleteFile(
        ex.companyLogo.split('/')[ex.companyLogo.split('/').length - 1],
      );
    }

    return this.experienceRepository.delete(id);
  }

  deleteFile(filename: string) {
    fs.unlink('./uploads/profile/' + filename, (err) => {
      console.log(err);
    });
  }
}
