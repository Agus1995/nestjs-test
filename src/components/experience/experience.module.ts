import { Module } from '@nestjs/common';
import { ExperienceService } from './experience.service';
import { ExperienceController } from './experience.controller';
import { MulterModule } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { Experience } from './entities/experience.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [
    TypeOrmModule.forFeature([Experience]),
    MulterModule.register({
      storage: diskStorage({
        destination: './uploads/logos',
        filename(req, file, callback) {
          callback(
            null,
            Date.now().toString() + '.' + file.originalname.split('.')[1],
          );
        },
      }),
    }),
  ],
  controllers: [ExperienceController],
  providers: [ExperienceService],
  exports: [ExperienceService],
})
export class ExperienceModule {}
