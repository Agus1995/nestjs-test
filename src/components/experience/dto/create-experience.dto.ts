export class CreateExperienceDto {
  id: string;
  jobTitle: string;
  company: string;
  description: string;
  resumeId: number;
  startDate: Date;
  endDate: Date;
}
