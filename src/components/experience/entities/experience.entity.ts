import { Resume } from 'src/components/resume/entities/resume.entity';
import {
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  Entity,
  JoinColumn,
} from 'typeorm';

@Entity()
export class Experience {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'job_title' })
  jobTitle: string;

  @Column()
  company: string;

  @Column({ name: 'company_logo' })
  companyLogo: string;

  @Column()
  description: string;

  @Column({ name: 'start_date', type: 'date' })
  startDate: Date;

  @Column({ name: 'end_date', type: 'date' })
  endDate: Date;

  @ManyToOne(() => Resume, (resume) => resume.id)
  @JoinColumn({ name: 'resume_id', referencedColumnName: 'id' })
  resume: Resume;
}
