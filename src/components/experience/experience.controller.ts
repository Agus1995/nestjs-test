import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseInterceptors,
  UploadedFile,
} from '@nestjs/common';
import { ExperienceService } from './experience.service';
import { CreateExperienceDto } from './dto/create-experience.dto';
import { UpdateExperienceDto } from './dto/update-experience.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { CommonResponse } from 'src/shared/CommonResponse';

@Controller('experience')
export class ExperienceController {
  constructor(private readonly experienceService: ExperienceService) {}

  @Post()
  @UseInterceptors(FileInterceptor('file'))
  async create(
    @Body() createExperienceDto: CreateExperienceDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    const data = await this.experienceService.create(createExperienceDto, file);
    const response = new CommonResponse(null, null, data);
    return response;
  }

  // @Get()
  // async findAll() {
  //   const data = await this.experienceService.findAll();
  //   const response = new CommonResponse(null, null, data);
  //   return response;
  // }

  // @Get(':id')
  // async findOne(@Param('id') id: string) {
  //   const data = await this.experienceService.findOne(+id);
  //   const response = new CommonResponse(null, null, data);
  //   return response;
  // }

  @Patch(':id')
  @UseInterceptors(FileInterceptor('file'))
  async update(
    @Param('id') id: string,
    @Body() updateExperienceDto: UpdateExperienceDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    const data = await this.experienceService.update(
      +id,
      updateExperienceDto,
      file,
    );
    const response = new CommonResponse(null, null, data);
    return response;
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    this.experienceService.remove(+id);
    return new CommonResponse(null, null, null);
  }
}
