import { Module } from '@nestjs/common';
import { ResumeService } from './resume.service';
import { ResumeController } from './resume.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Resume } from './entities/resume.entity';
import { MulterModule } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { ExperienceModule } from '../experience/experience.module';
import { UsersModule } from '../users/users.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Resume]),
    MulterModule.register({
      storage: diskStorage({
        destination: './uploads/profile',
        filename(req, file, callback) {
          callback(
            null,
            Date.now().toString() + '.' + file.originalname.split('.')[1],
          );
        },
      }),
    }),
    ExperienceModule,
    UsersModule,
  ],
  controllers: [ResumeController],
  providers: [ResumeService],
})
export class ResumeModule {}
