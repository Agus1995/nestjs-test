import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../users/entities/user.entity';
import { CreateResumeDto } from './dto/create-resume.dto';
import { UpdateResumeDto } from './dto/update-resume.dto';
import { Resume } from './entities/resume.entity';
import * as fs from 'fs';
import { ExperienceService } from '../experience/experience.service';
import { UsersService } from '../users/users.service';

@Injectable()
export class ResumeService {
  constructor(
    @InjectRepository(Resume)
    private resumeRepository: Repository<Resume>,
    private experienceService: ExperienceService,
    private userService: UsersService,
  ) {}

  async create(
    createResumeDto: CreateResumeDto,
    file: Express.Multer.File,
    req: any,
  ) {
    createResumeDto.profilePicture = file.filename;

    const resume = new Resume();
    const userInfo = new User();

    const decode = this.userService.decodeToken(
      req['authorization'].split(' ')[1],
    );

    userInfo.id = decode.id;

    resume.name = createResumeDto.name;
    resume.profilePicture = `http://localhost:3000/file/profile/${file.filename}`;
    resume.age = createResumeDto.age;
    resume.user = userInfo;

    const response = await this.resumeRepository.save(resume);
    return response;
  }

  findAll(req: any): Promise<Resume[]> {
    const decode = this.userService.decodeToken(
      req['authorization'].split(' ')[1],
    );

    return this.resumeRepository.find({
      relations: {
        experiences: true,
      },
      where: [
        {
          user: {
            id: decode.id,
          },
        },
      ],
    });
  }

  findOne(id: number): Promise<Resume> {
    return this.resumeRepository.findOne({
      where: {
        id,
      },
    });
  }

  async update(
    id: number,
    updateResumeDto: UpdateResumeDto,
    file: Express.Multer.File,
  ) {
    const beforeUpdate = await this.resumeRepository.findOne({
      where: {
        id,
      },
    });

    updateResumeDto.age ? (beforeUpdate.age = updateResumeDto.age) : null;
    updateResumeDto.name ? (beforeUpdate.name = updateResumeDto.name) : null;

    if (file) {
      this.deleteFile(
        beforeUpdate.profilePicture.split('/')[
          beforeUpdate.profilePicture.split('/').length - 1
        ],
      );
      beforeUpdate.profilePicture = `http://localhost:3000/file/${file.filename}`;
    }
    return this.resumeRepository.upsert(beforeUpdate, ['id']);
  }

  async remove(id: number) {
    const resume = await this.resumeRepository.findOne({
      where: {
        id,
      },
    });

    console.log(resume);

    this.deleteFile(
      resume.profilePicture.split('/')[
        resume.profilePicture.split('/').length - 1
      ],
    );

    if (resume && resume.experiences && resume.experiences.length) {
      resume.experiences.forEach((e) => {
        this.experienceService.remove(e.id);
      });
    }
    return this.resumeRepository.delete(id);
  }

  deleteFile(filename: string) {
    fs.unlink('./uploads/profile/' + filename, (err) => {
      console.log(err);
    });
  }
}
