export class CreateResumeDto {
  id: string;
  name: string;
  profilePicture: string;
  age: number;
}
