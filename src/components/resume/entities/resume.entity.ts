import { Experience } from 'src/components/experience/entities/experience.entity';
import { User } from 'src/components/users/entities/user.entity';
import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('resume')
export class Resume {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({ name: 'profile_picture' })
  profilePicture: string;

  @Column()
  age: number;

  @ManyToOne(() => User, (user) => user.id)
  user: User;

  @OneToMany(() => Experience, (ex) => ex.resume, {
    onDelete: 'CASCADE',
  })
  experiences: Experience[];
}
