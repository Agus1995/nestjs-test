import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseInterceptors,
  UploadedFile,
  UseGuards,
  Request,
  Headers,
} from '@nestjs/common';
import { ResumeService } from './resume.service';
import { CreateResumeDto } from './dto/create-resume.dto';
import { UpdateResumeDto } from './dto/update-resume.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { CommonResponse } from 'src/shared/CommonResponse';
import { JwtAuthGuard } from '../users/JwtAuthGuard';

@Controller('resume')
export class ResumeController {
  constructor(private readonly resumeService: ResumeService) {}

  @UseGuards(JwtAuthGuard)
  @Post()
  @UseInterceptors(FileInterceptor('file'))
  async create(
    @Body() createResumeDto: CreateResumeDto,
    @UploadedFile() file: Express.Multer.File,
    @Headers() request,
  ) {
    const data = await this.resumeService.create(
      createResumeDto,
      file,
      request,
    );
    const response = new CommonResponse(null, null, data);
    return response;
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  async findAll(@Headers() request) {
    const data = await this.resumeService.findAll(request);
    const response = new CommonResponse(null, null, data);
    return response;
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  async findOne(@Param('id') id: string) {
    const data = await this.resumeService.findOne(+id);
    const response = new CommonResponse(null, null, data);
    return response;
  }

  @UseGuards(JwtAuthGuard)
  @Patch(':id')
  @UseInterceptors(FileInterceptor('file'))
  async update(
    @Param('id') id: string,
    @Body() updateResumeDto: UpdateResumeDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    const data = await this.resumeService.update(+id, updateResumeDto, file);
    const response = new CommonResponse(null, null, data);
    return response;
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  remove(@Param('id') id: string) {
    this.resumeService.remove(+id);
    return new CommonResponse(null, null, null);
  }
}
