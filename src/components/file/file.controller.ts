import { Controller, Get, Param, Res, StreamableFile } from '@nestjs/common';
import { FileService } from './file.service';
import type { Response } from 'express';

@Controller('file')
export class FileController {
  constructor(private readonly fileService: FileService) {}

  @Get(':path/:filename')
  getFile(
    @Param('filename') filename: string,
    @Param('path') path: string,
    @Res({ passthrough: true }) res: Response,
  ): StreamableFile {
    const data = this.fileService.getFile(path, filename);
    res.set({
      'Content-Type': 'image/jpg',
      'Content-Disposition': `attachment; filename="${filename}"`,
    });
    return new StreamableFile(data);
  }
}
