import { Injectable, StreamableFile } from '@nestjs/common';
import { createReadStream } from 'fs';

@Injectable()
export class FileService {
  getFile(path: string, filename: string) {
    const file = createReadStream('./uploads/' + path + '/' + filename);
    return file;
  }
}
