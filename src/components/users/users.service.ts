import { Injectable, UnauthorizedException } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { use } from 'passport';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
    private jwtService: JwtService,
  ) {}

  async register(createUserDto: CreateUserDto) {
    const user = new User();
    user.password = await bcrypt.hash(createUserDto.password, 10);
    user.username = createUserDto.username;

    await this.userRepository.save(user);

    return true;
  }

  async login(createUserDto: CreateUserDto) {
    const user = await this.userRepository.findOne({
      where: {
        username: createUserDto.username,
      },
    });

    if (
      user &&
      bcrypt.compare(
        user.password,
        await bcrypt.hash(createUserDto.password, 10),
      )
    ) {
      const payload = {
        id: user.id,
        time: new Date(),
      };

      return { accessToken: this.jwtService.sign(payload) };
    }

    throw new UnauthorizedException();
  }

  decodeToken(token): any {
    return this.jwtService.decode(token);
  }
}
