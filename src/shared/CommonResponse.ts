export class CommonResponse<T> {
  responseCode: number;
  responseMessage: string;
  data: T;

  constructor(responseCode, responMessage, data) {
    this.responseCode = responseCode ? responseCode : 1; //if constructor params null, set with 00
    this.responseMessage = responMessage ? responMessage : 'Success';
    this.data = data;
  }
}
