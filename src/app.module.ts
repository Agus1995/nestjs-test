import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ResumeModule } from './components/resume/resume.module';
import { UsersModule } from './components/users/users.module';

import * as dotenv from 'dotenv';
import { MulterModule } from '@nestjs/platform-express';
import { ExperienceModule } from './components/experience/experience.module';
import { FileModule } from './components/file/file.module';
dotenv.config();

@Module({
  imports: [
    TypeOrmModule.forRoot({
      name: 'default',
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '1234',
      database: 'sonar-test',
      synchronize: true,
      // dropSchema: false,
      logging: true,
      entities: [__dirname + '/**/**/*.entity{.ts,.js}'],
    }),
    ResumeModule,
    UsersModule,
    ExperienceModule,
    FileModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
